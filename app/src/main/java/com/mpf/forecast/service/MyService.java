package com.mpf.forecast.service;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.os.IBinder;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.mpf.forecast.MainActivity;

import java.io.File;
import java.io.IOException;

public class MyService extends Service {
    MediaPlayer mp3;
    @Override
    public void onCreate() {
        super.onCreate();
        System.out.println("服务创建完成");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        System.out.println("服务运行中");
        String weather = intent.getStringExtra("weather");
        System.out.println("service:" + intent.getStringExtra("weather"));
        mp3 = new MediaPlayer();
        try {
            if(weather.equals("小雨"))
                mp3.setDataSource(this, Uri.parse("http://music.163.com/song/media/outer/url?id=430685732"));
            else if(weather.equals("阴")) {
                mp3.setDataSource(this, Uri.parse("http://music.163.com/song/media/outer/url?id=473742442"));
            } else if(weather.equals("晴")) {
                mp3.setDataSource(this, Uri.parse("http://music.163.com/song/media/outer/url?id=487527978"));
            } else {
                mp3.setDataSource(this, Uri.parse("http://music.163.com/song/media/outer/url?id=560184821"));
            }
            mp3.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }

        mp3.start();
//        MediaPlayer mediaPlayer = new MediaPlayer();
////        if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){
////            ActivityCompat.requestPermissions(MyService.this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},1);
////        }
//
////        new File(Environment.getExternalStorageDirectory())
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        System.out.println("服务已经停止");
        mp3.stop();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
