package com.mpf.forecast.bean;

import android.util.Log;

import com.mpf.forecast.utils.JDBCUtil;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class test {
    public static void main(String[] args) {
        Connection cn = JDBCUtil.getConnection();

        Statement st = null;
        ResultSet rs = null;

        //查
        try {
            String sql="select * from img";
            st = (Statement) cn.createStatement();
            rs=st.executeQuery(sql);
            while(rs.next()){
                String id = rs.getString("id");
                String imgName  = rs.getString("img_name");
                String imgUrl = rs.getString("img_url");
                String imgComment = rs.getString("img_comment");
                String imgPublished = rs.getString("img_published");
                String parentImgId = rs.getString("parent_img_id");
                System.out.println(id+imgName+imgUrl+imgComment+imgPublished+parentImgId);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close(cn,st,rs);
        }

        //增
//        try {
//            String sql="insert into img ";
//            st = (Statement) cn.createStatement();
//            rs=st.executeQuery(sql);
//            while(rs.next()){
//                String id = rs.getString("id");
//                String imgName  = rs.getString("img_name");
//                String imgUrl = rs.getString("img_url");
//                String imgComment = rs.getString("img_comment");
//                String imgPublished = rs.getString("img_published");
//                String parentImgId = rs.getString("parent_img_id");
//                System.out.println(id+imgName+imgUrl+imgComment+imgPublished+parentImgId);
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }finally {
//            JDBCUtil.close(cn,st,rs);
//        }


    }
}
