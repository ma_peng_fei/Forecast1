package com.mpf.forecast.bean;

public class ImgComment {

    //
    private Integer imgId;
    private String content;

    public Integer getImgId() {
        return imgId;
    }

    public void setImgId(int imgId) {
        this.imgId = imgId;
    }

    public ImgComment() {
    }

    public ImgComment(int imgId, String content) {
        this.imgId = imgId;
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
