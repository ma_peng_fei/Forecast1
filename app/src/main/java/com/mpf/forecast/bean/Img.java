package com.mpf.forecast.bean;


import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Img {

    private Integer id;
    private String imgName;
    private String imgUrl;

    private List<String> imgComments = new ArrayList<>();

    public Img(Integer id, String imgName, String imgUrl, List<String> imgComments, String imgPublished, String parentImgId) {
        this.id = id;
        this.imgName = imgName;
        this.imgUrl = imgUrl;
        this.imgComments = imgComments;
        this.imgPublished = imgPublished;
        this.parentImgId = parentImgId;
    }

    public List<String> getImgComments() {
        return imgComments;
    }

    public void setImgComments(List<String> imgComments) {
        this.imgComments = imgComments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Img img = (Img) o;
        return Objects.equals(id, img.id) &&
                Objects.equals(imgName, img.imgName) &&
                Objects.equals(imgUrl, img.imgUrl) &&
                Objects.equals(imgPublished, img.imgPublished) &&
                Objects.equals(parentImgId, img.parentImgId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, imgName, imgUrl, imgPublished, parentImgId);
    }

    private String imgPublished;
    private String parentImgId;

    public Img() {
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImgName() {
        return imgName;
    }

    public void setImgName(String imgName) {
        this.imgName = imgName;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getImgPublished() {
        return imgPublished;
    }

    public void setImgPublished(String imgPublished) {
        this.imgPublished = imgPublished;
    }

    public String getParentImgId() {
        return parentImgId;
    }

    public void setParentImgId(String parentImgId) {
        this.parentImgId = parentImgId;
    }
}
