package com.mpf.forecast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.mpf.forecast.city_manager.CityManagerActivity;
import com.mpf.forecast.db.DBManager;
import com.mpf.forecast.juhe.HttpUtils;
import com.mpf.forecast.juhe.URLUtils;
import com.mpf.forecast.receiver.MyReceiver;
import com.mpf.forecast.service.MyService;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private NotificationManager manager;

    ImageView addCityIv, moreIv, view; // whb 2
    LinearLayout pointLayout;
    RelativeLayout outLayout;
    ViewPager mainVp;
    //ViewPager的数据源
    List<Fragment> fragmentList;
    //表示需要显示的城市的集合
    List<String> cityList;
    //表示ViewPager的页数指数器显示集合
    List<ImageView> imgList;
    private CityFragmentPagerAdapter adapter;
    private SharedPreferences pref;
    private int bgNum;
    private int clickTime = 0;
    private String weather;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addCityIv = findViewById(R.id.main_iv_add);
        moreIv = findViewById(R.id.main_iv_more);
        view = findViewById(R.id.view);
        pointLayout = findViewById(R.id.main_layout_point);
        outLayout = findViewById(R.id.main_out_layout);
        exchangeBg();
        mainVp = findViewById(R.id.main_vp);
        view.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                SharedPreferences pref = getSharedPreferences("MyUsername", MODE_PRIVATE);
                if(pref.getString("username", null) == null) {
                    startActivity(new Intent(MainActivity.this, RegisterActivity.class));
                } else {
                    String username = pref.getString("username", null);
                    Toast.makeText(MainActivity.this, username, Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(MainActivity.this, BaseActivity.class));
                }
            }
        });
        //添加点击事件
        addCityIv.setOnClickListener(this);
        moreIv.setOnClickListener(this);

        fragmentList = new ArrayList<>();
        cityList = DBManager.queryAllCityName();//获取数据库包含的城市信息列表
        imgList = new ArrayList<>();

        if (cityList.size() == 0) {
            cityList.add("大连");
        }
        //因为可能搜索界面点击跳转到此界面会传值，所以此处获取一下
        Intent intent = getIntent();
        String city = intent.getStringExtra("city");
        if (!cityList.contains(city) && !TextUtils.isEmpty(city)) {
            cityList.add(city);
            System.out.println(city);
        }

        //初始化ViewPager页面
        initPager();
        adapter = new CityFragmentPagerAdapter(getSupportFragmentManager(), fragmentList);
        mainVp.setAdapter(adapter);
        //创建小圆点指示器
        initPoint();
        //设置最后一个城市信息
        mainVp.setCurrentItem(fragmentList.size() - 1);
        //设置ViewPager页面监听
        setPagerListener();

        MyReceiver myReceiver = new MyReceiver();
        registerReceiver(myReceiver, new IntentFilter(MyReceiver.ACTION));
        //发送广播
        sendBroadcast(new Intent(MyReceiver.ACTION));

        /**
         * Notification
         */
        manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelId = "weather";
            String channelName = "天气情况";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            createNotificationChannel(channelId, channelName, importance);

        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                String queryUrl = URLUtils.getTemp_url("大连");
                System.out.println(queryUrl);
                String jsonContent = HttpUtils.getJsonContent(queryUrl);
                try {
                    JSONObject jsonObject = new JSONObject(jsonContent);

                    String result = jsonObject.get("result").toString();
                    JSONObject jsonResult = new JSONObject(result);

                    String realtime = jsonResult.get("realtime").toString();
                    JSONObject jsonRealtime = new JSONObject(realtime);
                    weather = (String)jsonRealtime.get("info");

                    Notification notification = new NotificationCompat.Builder(MainActivity.this, "weather")
                            .setAutoCancel(true)
                            .setContentTitle("定位城市：" + jsonResult.get("city"))
                            //26℃ 晴 南风 2级
                            .setContentText("最近天气：" + jsonRealtime.get("temperature") + "℃" +" "+ jsonRealtime.get("info") +" "+  jsonRealtime.get("direct") + " "+ jsonRealtime.get("power"))
                            .setWhen(System.currentTimeMillis())
                            .setSmallIcon(R.mipmap.ic_launcher)
                            //设置红色
                            .setColor(Color.parseColor("#F00606"))
                            .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.icon))
//                        .setContentIntent(pendingIntent)
                            .build();
                    manager.notify(1, notification);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        final ImageView start_stop_music = (ImageView) findViewById(R.id.start_stop_music);
        start_stop_music.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                System.out.println(weather);
                clickTime += 1;
                Intent intent = new Intent(MainActivity.this, MyService.class);
                intent.putExtra("weather", weather);
                if(clickTime % 2 == 1) {
                    startService(intent);
                    start_stop_music.setImageResource(R.mipmap.music_on_play);
                } else {
                    stopService(intent);
                    start_stop_music.setImageResource(R.mipmap.music);
                }
            }
        });

    }

    @TargetApi(Build.VERSION_CODES.O)
    private void createNotificationChannel(String channelId, String channelName, int importance) {
        NotificationChannel channel = new NotificationChannel(channelId, channelName, importance);
        @SuppressLint("ServiceCast") NotificationManager notificationManager = (NotificationManager) getSystemService(
                NOTIFICATION_SERVICE);
        notificationManager.createNotificationChannel(channel);
    }

    //        换壁纸的函数
    public void exchangeBg() {
        pref = getSharedPreferences("bg_pref", MODE_PRIVATE);
        bgNum = pref.getInt("bg", 3);
        switch (bgNum) {
            case 0:
                outLayout.setBackgroundResource(R.mipmap.bg4);
                break;
            case 1:
                outLayout.setBackgroundResource(R.mipmap.bg3);
                break;
            case 2:
                System.out.println("2");
                outLayout.setBackgroundResource(R.mipmap.bg2);
                break;
            case 3:
                try {
                    FileInputStream stream = new FileInputStream("/sdcard/Pictures/fromPhoto.jpg");
                    Bitmap bitmap = BitmapFactory.decodeStream(stream);
                    System.out.println("获取到图片文件");
                    outLayout.setBackgroundDrawable(new BitmapDrawable(bitmap));
                } catch (FileNotFoundException e) {
                    System.out.println("未找到图片");
                }
                break;
        }

    }

    private void setPagerListener() {
        //设置监听事件
        mainVp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < imgList.size(); i++) {
                    imgList.get(i).setImageResource(R.mipmap.a1);
                }
                imgList.get(position).setImageResource(R.mipmap.a2);


                final int po = position;
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String city = cityList.get(po);
                        String queryUrl = URLUtils.getTemp_url(city);
                        System.out.println(queryUrl);
                        String jsonContent = HttpUtils.getJsonContent(queryUrl);
                        String musicWeather = "晴";
                        try {
                            JSONObject jsonObject = new JSONObject(jsonContent);

                            String result = jsonObject.get("result").toString();
                            JSONObject jsonResult = new JSONObject(result);

                            String realtime = jsonResult.get("realtime").toString();
                            JSONObject jsonRealtime = new JSONObject(realtime);
                            musicWeather = (String)jsonRealtime.get("info");

//                    Notification notification = new NotificationCompat.Builder(MainActivity.this, "weather")
//                            .setAutoCancel(true)
//                            .setContentTitle("定位城市：" + jsonResult.get("city"))
//                            //26℃ 晴 南风 2级
//                            .setContentText("最近天气：" + jsonRealtime.get("temperature") + "℃" +" "+ jsonRealtime.get("info") +" "+  jsonRealtime.get("direct") + " "+ jsonRealtime.get("power"))
//                            .setWhen(System.currentTimeMillis())
//                            .setSmallIcon(R.mipmap.ic_launcher)
//                            //设置红色
//                            .setColor(Color.parseColor("#F00606"))
//                            .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.icon))
////                        .setContentIntent(pendingIntent)
//                            .build();
//                    manager.notify(1, notification);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        final ImageView start_stop_music = (ImageView) findViewById(R.id.start_stop_music);
                        final String finalMusicWeather = musicWeather;
                        start_stop_music.setOnClickListener(new View.OnClickListener(){
                            @Override
                            public void onClick(View v) {
                                System.out.println(finalMusicWeather);
                                clickTime += 1;
                                Intent intent = new Intent(MainActivity.this, MyService.class);
                                intent.putExtra("weather", finalMusicWeather);
                                if(clickTime % 2 == 1) {
                                    startService(intent);
                                    start_stop_music.setImageResource(R.mipmap.music_on_play);
                                } else {
                                    stopService(intent);
                                    start_stop_music.setImageResource(R.mipmap.music);
                                }
                            }
                        });
                    }
                }).start();

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void initPoint() {
        //创建小圆点 ViewPager页面指示器的函数
        for (int i = 0; i < fragmentList.size(); i++) {
            ImageView pIv = new ImageView(this);
            pIv.setImageResource(R.mipmap.a1);
            pIv.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) pIv.getLayoutParams();
            layoutParams.setMargins(0, 0, 20, 0);
            imgList.add(pIv);
            pointLayout.addView(pIv);
        }
        imgList.get(imgList.size() - 1).setImageResource(R.mipmap.a2);
    }

    private void initPager() {
        System.out.println("citylist'size:" + cityList.size());
        //创建Fragment对象，添加到ViewPager数据源当中
        for (int i = 0; i < cityList.size(); i++) {
            CityWeatherFragment cwFrag = new CityWeatherFragment();
            Bundle bundle = new Bundle();
            bundle.putString("city", cityList.get(i));
            cwFrag.setArguments(bundle);
            fragmentList.add(cwFrag);
        }
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent();
        switch (view.getId()) {
            case R.id.main_iv_add:
                intent.setClass(this, CityManagerActivity.class);
                break;
            case R.id.main_iv_more:
                intent.setClass(this, MoreActivity.class);
                break;
        }
        startActivity(intent);
    }

    //当页面重写加载时会调用的函数，这个函数在页面获取焦点之前进行调用，此处完成ViewPager页数的更新
    @Override
    protected void onRestart() {
        super.onRestart();
        //获取数据库当中还剩下的城市列表
        List<String> list = DBManager.queryAllCityName();
        if (list.size() == 0) {
            list.add("大连");
        }
        cityList.clear();//重新加载之前，清空原来的数据源
        cityList.addAll(list);
        //剩余城市也要创建对应的fragment页面
        fragmentList.clear();
        initPager();
        adapter.notifyDataSetChanged();
        //页面数量发生改变，指示器的数量也会发生变化，重新设置添加指示器
        imgList.clear();
        pointLayout.removeAllViews();//将布局当中所有元素全部移出
        initPoint();
        mainVp.setCurrentItem(fragmentList.size() - 1);
    }
}
