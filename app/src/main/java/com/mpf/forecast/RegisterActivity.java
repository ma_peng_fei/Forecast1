package com.mpf.forecast;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Button registerBtn = (Button) findViewById(R.id.registerBtn);
        final EditText editText = (EditText) findViewById(R.id.editText);
        final SharedPreferences pref = getSharedPreferences("MyUsername", MODE_PRIVATE);
        final SharedPreferences.Editor editor = pref.edit();
        registerBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                editor.putString("username", String.valueOf(editText.getText()));
                editor.apply();
                finish();
            }
        });
    }
}