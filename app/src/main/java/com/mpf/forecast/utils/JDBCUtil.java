package com.mpf.forecast.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
public class JDBCUtil {
    //1.加载驱动
    static{
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    //获取远程mysql连接（url（主要用到的是ip+port+databaseName），user，password（主要用到的是phpMyadmin创建的可远程访问的用户的用户名和密码））
    private static String URL = "jdbc:mysql://47.103.209.145/book?characterEncoding=utf8";//通过jdbx访问mysql数据库服务器，远程服务器ip为192.168.43.160 ，port为3306，数据库名称为 test
    //phpMyadmin中创建的可远程访问权限的用户的用户名和密码
    private static String USER = "root";
    private static String PWD = "mapengfei411";

    //2.获取连接
    public static Connection getConnection(){

        Connection conn = null;
        try {
            conn = DriverManager.getConnection(URL, USER, PWD);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conn;
    }

    //3.关闭连接
    public static void close(Connection conn,Statement st,ResultSet rs){
        //关闭连接
        if(conn != null){
            try {
                conn.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        //关闭statement
        if(st != null){
            try {
                st.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        //关闭结果集
        if(rs != null){
            try {
                rs.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
}
