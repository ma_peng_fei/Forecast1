package com.mpf.forecast.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

//采用单例模式，确保一个类只有一个实例
//通过代码可以看出，懒汉单例模式在第一次调用 getInstance 方法时，实例化对象
//在类加载时并不自行实例化，这种技术称为延迟加载技术（Lazy Load），即在需要的时候进行加载实例
//采用双重检查锁定（Double-Check Locking）：在 synchronized 锁定的代码中再进行一次是否为null的判断，这样就可保证线程安全

public class ConfigUtil {


    //定义一个私有静态变量
    private static volatile ConfigUtil configUtil;

    //私有构造方法
    private ConfigUtil(){}

    //提供一个对外公用方法
    public static ConfigUtil getInstance(){
        //双重否定
        if(configUtil == null){
            //构造
            synchronized (ConfigUtil.class){
                if(configUtil == null){
                    configUtil = new ConfigUtil();
                }
            }
        }
        return configUtil;
    }

    /**
     *
     * @param path  配置文件的路径
     * @return
     */
    public Properties getProperties(String path){
        Properties properties = new Properties();

        try {
            InputStream resourceAsStream = new FileInputStream(new File(path));
            properties.load(resourceAsStream);
        } catch (Exception e) {
            System.out.println("配置文件"+path+"读取失败");
        }
        return properties;
    }

    public static void main(String[] args) {
        String path = "app/src/main/assets/db.properties";
        Properties properties = ConfigUtil.getInstance().getProperties(path);
        Set<Object> objects = properties.keySet();
        Iterator<Object> iterator = objects.iterator();
        while (iterator.hasNext()){
            String key = iterator.next().toString();
            String info = (String)properties.get(key);
            System.out.println(key + ":" + info);

        }
    }


}
