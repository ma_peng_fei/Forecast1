package com.mpf.forecast;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mpf.forecast.utils.JDBCUtil;
import com.squareup.picasso.Picasso;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ShowDetailActivity extends Activity {
    private List<Comment> commentList = new ArrayList<Comment>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_detail);
        // 获取意图对象
        Intent intent = getIntent();

        //获取传递的值
        final String[] imgInfos = intent.getStringArrayExtra("imgInfo");

        //设置值
        ImageView imageView = (ImageView) findViewById(R.id.showPic);
        TextView textView = (TextView) findViewById(R.id.showInfo);
        final EditText editText = (EditText) findViewById(R.id.editText);
        TextView photoTime = (TextView) findViewById(R.id.phototime);
        SharedPreferences pref = getSharedPreferences("MyUsername", MODE_PRIVATE);
        final String username = pref.getString("username", "游客");
//        TextView talk = (TextView) findViewById(R.id.talk);

        Picasso.with(this).load(imgInfos[0])
                .fit()
                .into(imageView);

        textView.setText(imgInfos[1] + imgInfos[2] + imgInfos[3]);
        String comments = imgInfos[3];
        comments = comments.substring(1, comments.length() - 1);
        photoTime.setText("拍摄于" + imgInfos[2]);
        String show_comment = "";
        final ArrayList commentlist=new ArrayList(Arrays.asList(comments.split(",")));
        Collections.reverse(commentlist);
        for(int i = 0; i < commentlist.size(); i++) {
            String one_comment = (String) commentlist.get(i);
            one_comment = one_comment.trim();
            ArrayList username_and_comment = new ArrayList(Arrays.asList(one_comment.split(":")));
            String userName, userComment;
            if(username_and_comment.size() > 1) {
                userName = (String) username_and_comment.get(0);
                userComment = (String) username_and_comment.get(1);
            } else {
                userName = "游客";
                userComment = (String) username_and_comment.get(0);
            }
            Comment comment = new Comment(userName, userComment);
            commentList.add(comment);
        }

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new CommentAdapter(commentList));
//        talk.setText(show_comment);

        Button button = (Button) findViewById(R.id.commit);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(editText.getText())) {
                    Toast.makeText(ShowDetailActivity.this, "输入内容为空！", Toast.LENGTH_SHORT).show();
                } else {
                    //显示提示框内容
//                    Toast.makeText(ShowDetailActivity.this, "您说：" + editText.getText(), Toast.LENGTH_SHORT).show();
                    //开启子线程，将用户内容存入数据库
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            Connection con = JDBCUtil.getConnection();
                            PreparedStatement ps = null;
                            ResultSet rs = null;

                            try {
                                String sql = "insert into img_comment(img_id,content) values(?,?)";
                                ps = con.prepareStatement(sql);

                                ps.setInt(1, Integer.parseInt(imgInfos[4]));
                                //赋值评论内容
                                ps.setString(2, username + ":" + editText.getText().toString());
                                //执行插入
                                int result = ps.executeUpdate();
                                if (result != 0) {
                                    System.out.println("插入成功！");
                                    //插入成功，重新刷新页面
                                    //通过handler传递更新信号，在主页面进行跳转

                                    //重新查询评论
                                    String reSelect = "select content from img_comment where img_id = ?";
                                    ps = con.prepareStatement(reSelect);

                                    ps.setInt(1, Integer.parseInt(imgInfos[4]));

                                    //执行插入
                                    ResultSet resultSet = ps.executeQuery();
                                    ArrayList<String> reComments = new ArrayList<>();
                                    while (resultSet.next()){
                                        String content = resultSet.getString("content");
                                        reComments.add(content);
                                    }
                                    //创建意图对象
                                    Intent intent = new Intent(ShowDetailActivity.this,ShowDetailActivity.class);
                                    String[] reInfos = new String[5];
                                    reInfos[0] = imgInfos[0];
                                    reInfos[1] = imgInfos[1];
                                    reInfos[2] = imgInfos[2];
                                    reInfos[3] = reComments.toString();
                                    reInfos[4] = imgInfos[4];
                                    //设置传递键值对
                                    intent.putExtra("imgInfo",reInfos);

                                    //激活意图
                                    startActivity(intent);
//                                    Intent intent1 = new Intent(ShowDetailActivity.this, BaseActivity.class);
//
//                                    startActivity(intent1);
                                }
                            } catch (SQLException e) {
                                e.printStackTrace();
                            } finally {
                                //关闭线程池
                                JDBCUtil.close(con, ps, rs);
                            }
                        }
                    }).start();
                }
            }
        });
    }
}
