package com.mpf.forecast;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ItemViewHolder> {

    private List<Comment> commentList;

    static class ItemViewHolder extends RecyclerView.ViewHolder {
        private TextView username;
        private TextView talk;

        public ItemViewHolder(View itemView) {
            super(itemView);
            username = (TextView) itemView.findViewById(R.id.username);
            talk = (TextView)itemView.findViewById(R.id.talk);
        }
    }

    public CommentAdapter(List<Comment> commentList) {
        this.commentList = commentList;
    }
    @Override
    public CommentAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_item, parent, false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(CommentAdapter.ItemViewHolder holder, int position) {
        Comment comment = commentList.get(position);
        holder.username.setText(comment.getUsername() + "：");
        holder.talk.setText(comment.getTalk());
    }

    @Override
    public int getItemCount() {
        return commentList.size();
    }
}
