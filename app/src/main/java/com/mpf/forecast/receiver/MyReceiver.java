package com.mpf.forecast.receiver;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import com.mpf.forecast.MainActivity;
import com.mpf.forecast.R;
import com.mpf.forecast.juhe.HttpUtils;
import com.mpf.forecast.juhe.URLUtils;

import org.json.JSONException;
import org.json.JSONObject;

public class MyReceiver extends BroadcastReceiver {
    public static String ACTION = "myReceiver";
    private NotificationManager manager;
    @Override
    public void onReceive(Context context, Intent intent) {
        Context context1 = null;
//        System.out.println("1111111111111111111");
        Toast.makeText(context,"收到一个广播信息",Toast.LENGTH_LONG).show();
        String queryUrl = URLUtils.getTemp_url("大连");
        System.out.println(queryUrl);
        String jsonContent = HttpUtils.getJsonContent(queryUrl);
        try {
            JSONObject jsonObject = new JSONObject(jsonContent);

            String result = jsonObject.get("result").toString();
            JSONObject jsonResult = new JSONObject(result);

            String realtime = jsonResult.get("realtime").toString();
            JSONObject jsonRealtime = new JSONObject(realtime);
            String weather = (String) jsonRealtime.get("info");


            Notification notification = new NotificationCompat.Builder(context1)
                    .setAutoCancel(true)
                    .setContentTitle("定位城市：" + jsonResult.get("city"))
                    //26℃ 晴 南风 2级
                    .setContentText("最近天气：" + jsonRealtime.get("temperature") + "℃" +" "+ jsonRealtime.get("info") +" "+  jsonRealtime.get("direct") + " "+ jsonRealtime.get("power"))
                    .setWhen(System.currentTimeMillis())
                    .setSmallIcon(R.mipmap.ic_launcher)
                    //设置红色
                    .setColor(Color.parseColor("#F00606"))
//                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.icon))
//                        .setContentIntent(pendingIntent)
                    .build();
            manager.notify(1, notification);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //Notification
//        NotificationCompat.Builder builder = new NotificationCompat.Builder();
    }
}
