package com.mpf.forecast.city_manager;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mpf.forecast.R;
import com.mpf.forecast.bean.WeatherBean;
import com.mpf.forecast.db.DatabaseBean;
import com.mpf.forecast.juhe.JHTempBean;

import java.util.List;

public class CityManagerAdapter extends BaseAdapter {
    Context context;
    List<DatabaseBean> mDatas;

    public CityManagerAdapter(Context context, List<DatabaseBean> mDatas) {
        this.context = context;
        this.mDatas = mDatas;
    }

    @Override
    public int getCount() {
        return mDatas.size();
    }

    @Override
    public Object getItem(int i) {
        return mDatas.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder = null;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_city_manager,null);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) view.getTag();
        }
        DatabaseBean bean = mDatas.get(i);
        viewHolder.cityTv.setText(bean.getCity());
        JHTempBean jhTempBean = new Gson().fromJson(bean.getContent(), JHTempBean.class);
        JHTempBean.ResultBean jhResult = jhTempBean.getResult();
        JHTempBean.ResultBean.FutureBean jhTodayBean = jhResult.getFuture().get(0);
        JHTempBean.ResultBean.RealtimeBean jhRealtime = jhResult.getRealtime();
        //获取今日天气情况
//        WeatherBean.ResultsBean.WeatherDataBean dataBean = weatherBean.getResults().get(0).getWeather_data().get(0);
        viewHolder.conTv.setText("天气："+jhRealtime.getInfo());
//        String[] split = dataBean.getDate().split("：");
//        String todayTemp = split[1].replace(")", "");
        viewHolder.currentTempTv.setText(jhRealtime.getTemperature()+"℃");
        viewHolder.windTv.setText(jhRealtime.getDirect()+jhRealtime.getPower());
        viewHolder.tempRangeTv.setText(jhTodayBean.getTemperature());
        return view;
    }

    class ViewHolder{
        TextView cityTv,conTv,currentTempTv,windTv,tempRangeTv;
        public ViewHolder(View itemView){
            cityTv = itemView.findViewById(R.id.item_city_tv_city);
            conTv = itemView.findViewById(R.id.item_city_tv_condition);
            currentTempTv = itemView.findViewById(R.id.item_city_tv_temp);
            windTv = itemView.findViewById(R.id.item_city_wind);
            tempRangeTv = itemView.findViewById(R.id.item_city_temprange);
        }
    }
}
