package com.mpf.forecast;

public class Comment {
    private String username;
    private String talk;

    public Comment(String username, String talk) {
        this.username = username;
        this.talk = talk;
    }
    public String getUsername() {return username;}
    public String getTalk() {return talk;}
}
