package com.mpf.forecast.juhe;

// 字符串的拼接地址
public class URLUtils {

    //我的
//    public static final String KEY ="193978c7b58471707bf22cdd5562aab8";
    //海博的 cd33e190d9a3d662df957f0c28e682a1
    public static final String KEY ="193978c7b58471707bf22cdd5562aab8";
    public static String  temp_url ="http://apis.juhe.cn/simpleWeather/query";

    public static String index_url ="http://apis.juhe.cn/simpleWeather/life";

    public static String getTemp_url(String city){
        String url = temp_url+"?city="+city+"&key="+KEY;
        return url;
    }

    public static String getIndex_url(String city){
        String url = index_url+"?city="+city+"&key="+KEY;
        return url;
    }
}

