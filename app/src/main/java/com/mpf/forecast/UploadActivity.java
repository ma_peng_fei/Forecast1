package com.mpf.forecast;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;

import com.mpf.forecast.utils.JDBCUtil;
import com.mpf.forecast.utils.UploadUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class UploadActivity  extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        //
        super.onCreate(savedInstanceState);
        setContentView(R.layout.upload_img);
        Intent photoIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(photoIntent, 1);
        System.out.println("choose from the photos");
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.out.println("从相册中获取到照片");
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1) {
            if(resultCode == RESULT_OK) {
                Uri uri = data.getData();
                Log.e("图片URI:", uri.toString());
                ContentResolver cr = this.getContentResolver();
                try {
                    Bitmap bitmap = BitmapFactory.decodeStream(cr.openInputStream(uri));
                    saveBitmap(bitmap, "myView");
                    //开启子线程 访问数据库
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            UploadUtil uploadUtil = new UploadUtil(UploadActivity.this);
                            String result = uploadUtil.uploadUser("/sdcard/Pictures/myView.jpg");
                            System.out.println(result);

                            try {
                                Connection con = JDBCUtil.getConnection();
                                PreparedStatement ps = null;
                                ResultSet rs = null;

                                //将图片信息插入数据库中
                                try {
                                    String sql = "insert into img(img_name,img_url,img_published,parent_img_id) values(?,?,?,?)";
                                    ps = con.prepareStatement(sql);

                                    ps.setString(1,"test");
                                    ps.setString(2,result);


                                    ps.setObject(3,new Date());
                                    ps.setObject(4,0);

                                    int executeUpdate = ps.executeUpdate();

                                    if(executeUpdate!=0){
                                        System.out.println("success!!!!!!!!!!!");
                                        Intent intent = new Intent(UploadActivity.this,BaseActivity.class);
                                        startActivity(intent);
                                    }else {
                                        System.out.println("error!!!!!!!!!!!!!");
                                    }
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                } finally {
                                    //关闭线程池
                                    JDBCUtil.close(con, ps, rs);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }).start();
                }catch(FileNotFoundException e) {
                    Log.e("出错了：", e.getMessage());
                }
            }
        }
    }

    public void saveBitmap(Bitmap bitmap, String picName) {
        File f = new File("/sdcard/Pictures/", picName + ".jpg");
        if (f.exists()) {
            f.delete();
        }
        try {
            FileOutputStream out = new FileOutputStream(f);
            bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.flush();
            out.close();
            System.out.println(f.getPath());
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
