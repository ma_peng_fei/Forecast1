package com.mpf.forecast;

import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;

import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.mpf.forecast.base.BaseFragment;
import com.mpf.forecast.bean.WeatherBean;
import com.mpf.forecast.db.DBManager;
import com.mpf.forecast.juhe.HttpUtils;
import com.mpf.forecast.juhe.JHIndexBean;
import com.mpf.forecast.juhe.JHTempBean;
import com.mpf.forecast.juhe.URLUtils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class CityWeatherFragment extends BaseFragment implements View.OnClickListener {
    TextView tempTv,cityTv,conditionTv,windTv,tempRangeTv,dateTv,clothIndexTv,carIndexTv,coldIndexTv,sportIndexTv,raysIndexTv,airIndexTv;
    ImageView dayIv;
    LinearLayout futureLayout;
    ScrollView outLayout;
    JHIndexBean.ResultBean.LifeBean lifeBean; //指数信息
    //切割天气API
//    String url1 = "http://api.map.baidu.com/telematics/v3/weather?location=";
//    String url2 = "&output=json&ak=FkPhtMBK0HTIQNh7gG4cNUttSTyr0nzo";
//    private List<?> indexList;
    //获取指数信息集合
    private List<WeatherBean.ResultsBean.WeatherDataBean> weather_data;
    String city;
    private SharedPreferences pref;
    private int bgNum;

    //        换壁纸的函数
    public void exchangeBg(){
        pref = getActivity().getSharedPreferences("bg_pref", MODE_PRIVATE);
        bgNum = pref.getInt("bg", 3);
        switch (bgNum) {
            case 0:
                outLayout.setBackgroundResource(R.mipmap.bg4);
                break;
            case 1:
                outLayout.setBackgroundResource(R.mipmap.bg3);
                break;
            case 2:
                outLayout.setBackgroundResource(R.mipmap.bg2);
                break;
            case 3:
                try {
                    FileInputStream stream = new FileInputStream("/sdcard/Pictures/fromPhoto.jpg");
                    Bitmap bitmap = BitmapFactory.decodeStream(stream);
                    System.out.println("获取到图片文件");
                    outLayout.setBackgroundDrawable(new BitmapDrawable(bitmap));
                }catch(FileNotFoundException e) {
                    System.out.println("未找到图片");
                }
                break;
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_city_weather, container, false);
        initView(view);
        exchangeBg();
        //通过bundle获取activity信息
        Bundle bundle = getArguments();
        city = bundle.getString("city");
        String url = URLUtils.getTemp_url(city);
        //调用父类获取数据的方法
        loadData(url);

        //获取指数信息的网址
        String index_url = URLUtils.getIndex_url(city);
        loadIndexData(index_url);
        return view;
    }



    /*网络获取指数信息*/
    private void loadIndexData(final String index_url) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String json = HttpUtils.getJsonContent(index_url);
                JHIndexBean jhIndexBean = new Gson().fromJson(json, JHIndexBean.class);
                lifeBean = jhIndexBean.getResult().getLife();
            }
        }).start();
    }

    @Override
    public void onSuccess(String result) {
        //解析并展示数据
        parseShowData(result);
        int i = DBManager.updateInfoByCity(city, result);
        if (i<=0) {
//            更新数据库失败，说明没有这条城市信息，增加这个城市记录
            DBManager.addCityInfo(city,result);
        }
    }

    @Override
    public void onError(Throwable ex, boolean isOnCallback) {
//        数据库当中查找上一次信息显示在Fragment当中
        String s = DBManager.queryInfoByCity(city);
        if (!TextUtils.isEmpty(s)) {
            parseShowData(s);
        }
    }

    private void parseShowData(String result) {
        //使用gson解析数据
//        WeatherBean weatherBean = new Gson().fromJson(result, WeatherBean.class);
//        WeatherBean.ResultsBean resultsBean = weatherBean.getResults().get(0);
        JHTempBean jhTempBean = new Gson().fromJson(result, JHTempBean.class);
        JHTempBean.ResultBean jhResult = jhTempBean.getResult();
        //获取指数信息集合列表
//        indexList = resultsBean.getIndex();
//        weather_data = resultsBean.getWeather_data();
        //设置TextView
        dateTv.setText(jhResult.getFuture().get(0).getDate());
        cityTv.setText(jhResult.getCity());
        //获取今日天气情况
//        WeatherBean.ResultsBean.WeatherDataBean todayDataBean = resultsBean.getWeather_data().get(0);
        JHTempBean.ResultBean.FutureBean jhTodayFuture = jhResult.getFuture().get(0);
        JHTempBean.ResultBean.RealtimeBean jhRealtime = jhResult.getRealtime();

        windTv.setText(jhRealtime.getDirect()+jhRealtime.getPower());
        tempRangeTv.setText(jhTodayFuture.getTemperature());
        conditionTv.setText(jhRealtime.getInfo());
        //获取实时天气温度情况
        tempTv.setText(jhRealtime.getTemperature()+"℃");
        //设置显示的天气情况图片
//        Picasso.with(getActivity()).load(todayDataBean.getDayPictureUrl()).into(dayIv);
        //获取未来三天的天气情况，加载到layout中
//        List<WeatherBean.ResultsBean.WeatherDataBean> futureList = resultsBean.getWeather_data();
        List<JHTempBean.ResultBean.FutureBean> futureList = jhResult.getFuture();
        futureList.remove(0);
        for (int i = 0; i < futureList.size(); i++) {
            //对于一个没有被载入或者想要动态载入的界面，都需要使用LayoutInflater.inflate()来载入；
            View itemView = LayoutInflater.from(getActivity()).inflate(R.layout.item_main_center, null);
            itemView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT));
            futureLayout.addView(itemView);
            TextView idateTv = itemView.findViewById(R.id.item_center_tv_date);
            TextView iconTv = itemView.findViewById(R.id.item_center_tv_con);
            TextView windTv = itemView.findViewById(R.id.item_city_tv_wind);
            TextView itemprangeTv = itemView.findViewById(R.id.item_center_tv_temp);
            ImageView iIv = itemView.findViewById(R.id.item_center_iv);
            //获取对应的位置的天气情况
            JHTempBean.ResultBean.FutureBean dataBean = futureList.get(i);
            idateTv.setText(dataBean.getDate());
            iconTv.setText(dataBean.getWeather());
            windTv.setText(dataBean.getDirect());
            itemprangeTv.setText(dataBean.getTemperature());
//            Picasso.with(getActivity()).load(dataBean.getDayPictureUrl()).into(iIv);
        }
    }

    private void initView(View view) {
        //用于初始化控件操作
        tempTv = view.findViewById(R.id.frag_tv_currenttemp);
        cityTv = view.findViewById(R.id.frag_tv_city);
        conditionTv = view.findViewById(R.id.frag_tv_condition);
        windTv = view.findViewById(R.id.frag_tv_wind);
        tempRangeTv = view.findViewById(R.id.frag_tv_temprange);
        dateTv = view.findViewById(R.id.frag_tv_date);
        clothIndexTv = view.findViewById(R.id.frag_index_tv_dress);
        carIndexTv = view.findViewById(R.id.frag_index_tv_washcar);
        coldIndexTv = view.findViewById(R.id.frag_index_tv_cold);
        sportIndexTv = view.findViewById(R.id.frag_index_tv_sport);
        raysIndexTv = view.findViewById(R.id.frag_index_tv_rays);
        airIndexTv = view.findViewById(R.id.frag_index_tv_air);
        dayIv = view.findViewById(R.id.frag_iv_today);
        futureLayout = view.findViewById(R.id.frag_future_layout);
        outLayout = view.findViewById(R.id.out_layout);
        //设置监听
        clothIndexTv.setOnClickListener(this);
        carIndexTv.setOnClickListener(this);
        coldIndexTv.setOnClickListener(this);
        sportIndexTv.setOnClickListener(this);
        raysIndexTv.setOnClickListener(this);
        airIndexTv.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        switch (v.getId()) {
            case R.id.frag_index_tv_dress:
                builder.setTitle("穿衣指数");
                String msg = "暂无信息";
                if(lifeBean != null){
                    msg = lifeBean.getChuanyi().getV()+"\n"+lifeBean.getChuanyi().getDes();
                }
                builder.setMessage(msg);
                builder.setPositiveButton("确定",null);
                break;
            case R.id.frag_index_tv_washcar:
                builder.setTitle("洗车指数");
                msg = "暂无信息";
                if(lifeBean != null){
                    msg = lifeBean.getXiche().getV()+"\n"+lifeBean.getXiche().getDes();
                }
                builder.setMessage(msg);
                builder.setPositiveButton("确定",null);
                break;
            case R.id.frag_index_tv_cold:
                builder.setTitle("感冒指数");
                msg = "暂无信息";
                if(lifeBean != null){
                    msg = lifeBean.getGanmao().getV()+"\n"+lifeBean.getGanmao().getDes();
                }
                builder.setMessage(msg);
                builder.setPositiveButton("确定",null);
                break;
            case R.id.frag_index_tv_sport:
                builder.setTitle("运动指数");
                msg = "暂无信息";
                if(lifeBean != null){
                    msg = lifeBean.getYundong().getV()+"\n"+lifeBean.getYundong().getDes();
                }
                builder.setMessage(msg);
                builder.setPositiveButton("确定",null);
                break;
            case R.id.frag_index_tv_rays:
                builder.setTitle("紫外线指数");
                msg = "暂无信息";
                if(lifeBean != null){
                    msg = lifeBean.getZiwaixian().getV()+"\n"+lifeBean.getZiwaixian().getDes();
                }
                builder.setMessage(msg);
                builder.setPositiveButton("确定",null);
                break;
            case R.id.frag_index_tv_air:
                builder.setTitle("空调指数");
                msg = "暂无信息";
                if(lifeBean != null){
                    msg = lifeBean.getKongtiao().getV()+"\n"+lifeBean.getKongtiao().getDes();
                }
                builder.setMessage(msg);
                builder.setPositiveButton("确定",null);
                break;
        }
        builder.create().show();
    }
}