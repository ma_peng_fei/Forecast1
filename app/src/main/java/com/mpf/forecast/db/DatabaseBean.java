package com.mpf.forecast.db;

public class DatabaseBean {
    private int id;
    private String city;

    public DatabaseBean() {
    }

    public int getId() {
        return id;
    }

    public DatabaseBean(int id, String city, String content) {
        this.id = id;
        this.city = city;
        this.content = content;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    private String content;
}
