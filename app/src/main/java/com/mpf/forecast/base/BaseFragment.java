package com.mpf.forecast.base;



import androidx.fragment.app.Fragment;

import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.x;

/**
 * xutils加载网络数据的步骤
 * 1.声明xutils整体模块框架
 * 2.执行网络请求操作
 */

public class BaseFragment extends Fragment implements Callback.CommonCallback<String> {

    public void loadData(String path){
        //封装请求网络路径
        RequestParams requestParams = new RequestParams(path);
        //执行网络请求操作
        x.http().get(requestParams,this);
    }

    //请求成功时，会回调的接口
    @Override
    public void onSuccess(String result) {
        //解析并展示数据

    }

    //请求失败时，会回调的接口
    @Override
    public void onError(Throwable ex, boolean isOnCallback) {

    }

    //取消请求时，会回调的接口
    @Override
    public void onCancelled(CancelledException cex) {

    }

    //请求完成时，会回调的接口
    @Override
    public void onFinished() {

    }
}
