package com.mpf.forecast.base;

import android.app.Application;

import com.mpf.forecast.db.DBManager;

import org.xutils.x;

public class UnitApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        x.Ext.init(this);
        //初始化数据库
        DBManager.initDB(this);
    }
}
