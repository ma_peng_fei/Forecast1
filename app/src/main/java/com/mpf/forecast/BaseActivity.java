package com.mpf.forecast;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.mpf.forecast.bean.Img;
import com.mpf.forecast.bean.ImgComment;
import com.mpf.forecast.utils.JDBCUtil;
import com.squareup.picasso.Picasso;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;


public class BaseActivity extends Activity {
    //存放img信息
    private ArrayList<Img> imgs = new ArrayList<>();

    Thread db = null;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.item1:
                Intent intent = new Intent(BaseActivity.this, UploadActivity.class);
                startActivity(intent);
                break;
            case R.id.item2:
                startActivity(new Intent(BaseActivity.this, BaseActivity.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.mainmenu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_img);

        //获取RecycleView组件
        RecyclerView rv = (RecyclerView) findViewById(R.id.recyclerView);
//        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setLayoutManager(new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL));
        rv.addItemDecoration(new SpacesItemDecoration(1));
        //设置适配器，将RecycleView组件与item进行绑定
        rv.setAdapter(new RecyclerView.Adapter() {
            class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
                private TextView img_title;
                private TextView img_published;
                private ImageView img_Url;
                private TextView img_comments;

                public MyViewHolder(View itemView) {
                    super(itemView);
                    img_title = (TextView) itemView.findViewById(R.id.img_title);
                    img_published = (TextView) itemView.findViewById(R.id.img_published);
                    img_Url = (ImageView) itemView.findViewById(R.id.imgView);
                    img_comments = (TextView) itemView.findViewById(R.id.img_comments);

                }


                public TextView getImg_title() {
                    return img_title;
                }

                public TextView getImg_published() {
                    return img_published;
                }

                public ImageView getImg_Url() {
                    return img_Url;
                }

                public TextView getImg_comments(){
                    return img_comments;
                }

                @Override
                public void onClick(View view) {

                }
            }

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
                return new MyViewHolder(LayoutInflater.from(BaseActivity.this).inflate(R.layout.list_img, null));
            }


            ArrayList<String> tempList;
            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

                //绑定操作

                try {
                    db.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                MyViewHolder myHolder = (MyViewHolder) holder;

                Img img = imgs.get(position);
                myHolder.getImg_title().setText(img.getImgName());
                myHolder.getImg_published().setText(img.getImgPublished());

                myHolder.getImg_comments().setText(img.getImgComments().toString());

                //利用Picasso框架加载图片
                Picasso.with(BaseActivity.this).load(imgs.get(position).getImgUrl())
                        .fit()
                        .into(myHolder.getImg_Url());

                //给RecycleView组件的item设置监听事件
                myHolder.getImg_Url().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(view.getContext(),imgs.get(position).getImgName(),Toast.LENGTH_LONG).show();

                        //获取对应点击的图片信息
                        Img img1 = imgs.get(position);
                        //使用数组来存储点击的图片信息，方便在跳转activity时传递信息
                        String[] infos = new String[5];
                        infos[0] = img1.getImgUrl();
                        infos[1] = img1.getImgName();
                        infos[2] = img1.getImgPublished();
                        infos[3] = img1.getImgComments().toString();
                        infos[4] = img1.getId().toString();

                        //创建意图对象
                        Intent intent = new Intent(BaseActivity.this,ShowDetailActivity.class);
                        //设置传递键值对
                        intent.putExtra("imgInfo",infos);

                        //激活意图
                        startActivity(intent);
                    }
                });
            }


            //设置RecycleView加载的item数量
            @Override
            public int getItemCount() {
                try {
                    db.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return imgs.size();
            }
        });


        //JDBC访问数据库在主线程中会报错，因此重新new一个子线程进行访问
        db = new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    //利用封装好的JDBCUtil提供Connection
                    Connection con = JDBCUtil.getConnection();
                    //查询sql语句
                    String sql = "SELECT DISTINCT img.*,img_comment.content FROM img LEFT JOIN img_comment ON img.id = img_comment.img_id";
                    Statement st = (Statement) con.createStatement();
                    ResultSet rs = st.executeQuery(sql);

                    //用于存放图片评论
                    List<ImgComment> comments = new ArrayList<>();

//                    Message msg = new Message();

                    //遍历查询结果
                    while (rs.next()) {

                        //取出对应数据项信息
                        String id = rs.getString("id");
                        String imgName = rs.getString("img_name");
                        String imgUrl = rs.getString("img_url");
                        String imgPublished = rs.getString("img_published");
                        String commentContent = rs.getString("content");

//                        msg.what = 1;

                        //图片信息存入对应img实体中
                        Img img = new Img();
                        img.setId(Integer.valueOf(id));
                        img.setImgName(imgName);
                        img.setImgUrl(imgUrl);
                        img.setImgPublished(imgPublished);
                        imgs.add(img);

                        //图片评论内容存入对应实体中
                        ImgComment imgComment = new ImgComment();
                        imgComment.setImgId(Integer.valueOf(id));
                        imgComment.setContent(commentContent);

                        comments.add(imgComment);


                    }

                    //imgs去重
                    for (int i = 0; i < imgs.size() - 1; i++) {
                        for (int j = imgs.size() - 1; j > i; j--) {
                            if (imgs.get(i).equals(imgs.get(j)))
                                imgs.remove(j);
                        }
                    }

                    //遍历imgs集合 查询对应评论 封装到对应实体中
                    for (int i = 0; i < imgs.size(); i++) {
                        Img img = imgs.get(i);

                        for (int j = 0; j < comments.size(); j++) {
                            ImgComment imgComment = comments.get(j);
                            if(imgComment.getImgId().equals(img.getId())){

                                List<String> everyImgComments = img.getImgComments();
                                everyImgComments.add(imgComment.getContent());

                            }
                        }
                    }

                    //至此已查询得到所有数据并封装到imgs集合中。

                    //关闭JDBC连接
                    JDBCUtil.close(con, st, rs);

                }catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });
        //开启线程
        db.start();
        //获取上传图片按钮
        ImageView btn_more_photo = (ImageView) findViewById(R.id.add_more_photo);
        btn_more_photo.setOnClickListener(new View.OnClickListener(){

            //进行页面跳转更新
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BaseActivity.this, UploadActivity.class);
                startActivity(intent);
            }
        });
    }
}
